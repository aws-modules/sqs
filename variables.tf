variable "environment" {
  type = string
}
variable "product" {
  type = string
}
variable "application" {
  type    = string
  default = null
}
variable "use_case" {
  type = string
}
variable "need_dlq" {
  type    = bool
  default = false
}
variable "dlq_deadLetterTargetArn" {
  type    = string
  default = null
}
variable "dlq_maxReceiveCount" {
  type    = number
  default = 5
}
variable "visibility_timeout_seconds" {
  type    = number
  default = 300
}
variable "message_retention_seconds" {
  type    = number
  default = 345600
}
variable "max_message_size" {
  type    = number
  default = 262144
}
variable "delay_seconds" {
  type    = number
  default = 0
}
variable "receive_wait_time_seconds" {
  type    = number
  default = 0
}
variable "fifo_queue" {
  type    = bool
  default = true
}
variable "content_based_deduplication" {
  type    = bool
  default = true
}
variable "kms_master_key_id" {
  type    = string
  default = "alias/aws/sqs"
}
variable "kms_data_key_reuse_period_seconds" {
  type    = number
  default = 300
}
variable "deduplication_scope" {
  type    = string
  default = "queue"
}
variable "fifo_throughput_limit" {
  type    = string
  default = "perQueue"
}