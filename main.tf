resource "aws_sqs_queue" "this" {
  name = var.application != null ? "${var.environment}_${var.product}_${var.application}_${var.use_case}" : "${var.environment}_${var.product}_${var.use_case}"
  redrive_policy = var.need_dlq ? jsonencode(
    {
      deadLetterTargetArn = var.dlq_deadLetterTargetArn
      maxReceiveCount     = var.dlq_maxReceiveCount
    }
  ) : null
  visibility_timeout_seconds        = var.visibility_timeout_seconds
  message_retention_seconds         = var.message_retention_seconds
  max_message_size                  = var.max_message_size
  delay_seconds                     = var.delay_seconds
  receive_wait_time_seconds         = var.receive_wait_time_seconds
  fifo_queue                        = var.fifo_queue
  content_based_deduplication       = var.content_based_deduplication
  kms_master_key_id                 = var.kms_master_key_id
  kms_data_key_reuse_period_seconds = var.kms_data_key_reuse_period_seconds
  deduplication_scope               = var.deduplication_scope
  fifo_throughput_limit             = var.fifo_throughput_limit
  tags = var.application != null ? {
    Environment             = var.environment
    Product                 = var.product
    Application             = var.application
    Use_case                = var.use_case
    Can_be_deleted          = true
    Created_using_terraform = true
    } : {
    Environment             = var.environment
    Product                 = var.product
    Use_case                = var.use_case
    Can_be_deleted          = true
    Created_using_terraform = true
  }
}